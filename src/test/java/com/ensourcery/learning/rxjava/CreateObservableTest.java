package com.ensourcery.learning.rxjava;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.observers.TestObserver;
import io.reactivex.rxjava3.schedulers.TestScheduler;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

public class CreateObservableTest {

    @Test
    void range() {
        @NonNull Observable<Integer> observable = Observable.range(7, 5);
        observable.test().assertResult(7, 8, 9, 10, 11);
    }

    @Test
    void rangeLong() {
        @NonNull Observable<Long> observable = Observable.rangeLong(7, 5);
        observable.test().assertResult(7L, 8L, 9L, 10L, 11L);
    }

    @Test
    void intervalRange() {
        TestScheduler testScheduler = new TestScheduler();
        @NonNull Observable<Long> observable = Observable.intervalRange(7L, 5L, 1, 1, TimeUnit.SECONDS, testScheduler);
        @NonNull TestObserver<Long> testObserver = observable.test();

        testObserver.assertEmpty();

        testScheduler.advanceTimeBy(3, TimeUnit.SECONDS);
        testObserver.assertValues(7L, 8L, 9L);
        testObserver.assertNoErrors();
        testObserver.assertNotComplete();

        testScheduler.advanceTimeBy(2, TimeUnit.SECONDS);
        testObserver.assertValues(7L, 8L, 9L, 10L, 11L);
        testObserver.assertNoErrors();
        testObserver.assertComplete();
    }

    @Test
    void interval() {
        TestScheduler testScheduler = new TestScheduler();
        @NonNull Observable<Long> observable = Observable.interval(2L, 3L, TimeUnit.SECONDS, testScheduler);
        @NonNull TestObserver<Long> testObserver = observable.test();

        testObserver.assertEmpty();

        testScheduler.advanceTimeBy(2, TimeUnit.SECONDS);
        testObserver.assertValues(0L);
        testObserver.assertNoErrors();

        testScheduler.advanceTimeBy(6, TimeUnit.SECONDS);
        testObserver.assertValues(0L, 1L, 2L);
        testObserver.assertNoErrors();
    }
}
